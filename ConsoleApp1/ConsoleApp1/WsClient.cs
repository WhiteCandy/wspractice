﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Collections;
using ConsoleApp1;

namespace ConsoleApp1
{
    class WsClient
    {
        private WsServer Owner { set; get; }
        private int ClientId { set; get; }
        private TcpClient Client { set; get; }
        private byte[] ReadBuffer { set; get; }

        public WsClient(WsServer owner, int clientId, TcpClient client)
        {
            Owner = owner;
            ClientId = clientId;
            Client = client;
            ReadBuffer = new byte[client.Available];
        }

        public void OnConnect(AsyncCallback callback)
        {
            var stream = Client.GetStream();
            ReadBuffer = new byte[Client.Available];
            stream.BeginRead(ReadBuffer, 0, ReadBuffer.Length, callback, this);
        }

        private void DisConnect()
        {
            Console.WriteLine("{0} Client Disconnect", ClientId);
            Client.Close();

            Owner.OnDisConnect(ClientId);
        }

        public void OnAccept(IAsyncResult ar)
        {
            int recvLength = Client.GetStream().EndRead(ar);

            if (recvLength <= 0)
            {
                Client.Close();
                Console.WriteLine("disconnect");
                return;
            }

            string s = Encoding.UTF8.GetString(ReadBuffer);

            if (false == Regex.IsMatch(s, "^GET", RegexOptions.IgnoreCase))
            {
                Client.Close();
                Console.WriteLine("invalid Connect");
                return;
            }

            // 1. Obtain the value of the "Sec-WebSocket-Key" request header without any leading or trailing whitespace
            // 2. Concatenate it with "258EAFA5-E914-47DA-95CA-C5AB0DC85B11" (a special GUID specified by RFC 6455)
            // 3. Compute SHA-1 and Base64 hash of the new value
            // 4. Write the hash back as the value of "Sec-WebSocket-Accept" response header in an HTTP response
            string swk = Regex.Match(s, "Sec-WebSocket-Key: (.*)").Groups[1].Value.Trim();
            string swka = swk + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
            byte[] swkaSha1 = System.Security.Cryptography.SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(swka));
            string swkaSha1Base64 = Convert.ToBase64String(swkaSha1);

            // HTTP/1.1 defines the sequence CR LF as the end-of-line marker
            byte[] response = Encoding.UTF8.GetBytes(
                "HTTP/1.1 101 Switching Protocols\r\n" +
                "Connection: Upgrade\r\n" +
                "Upgrade: websocket\r\n" +
                "Sec-WebSocket-Accept: " + swkaSha1Base64 + "\r\n\r\n");

            Client.GetStream().Write(response, 0, response.Length);

            Client.GetStream().BeginRead(ReadBuffer, 0, ReadBuffer.Length, OnRecv, null);
        }
        private void OnRecv(IAsyncResult ar)
        {
            int recvLength = Client.GetStream().EndRead(ar);

            if (recvLength == 8)
            {
                Console.WriteLine("disconnect");
                Client.Close();
                return;
            }

            if (recvLength == 0)
            {
                Client.Close();
                return;
            }

            bool fin = (ReadBuffer[0] & 0b10000000) != 0,
                    mask = (ReadBuffer[1] & 0b10000000) != 0; // must be true, "All messages from the client to the server have this bit set"

            int opcode = ReadBuffer[0] & 0b00001111, // expecting 1 - text message
                msglen = ReadBuffer[1] - 128, // & 0111 1111
                offset = 2;

            if (msglen == 126)
            {
                // was ToUInt16(bytes, offset) but the result is incorrect
                msglen = BitConverter.ToUInt16(new byte[] { ReadBuffer[3], ReadBuffer[2] }, 0);
                offset = 4;
            }
            else if (msglen == 127)
            {
                Console.WriteLine("TODO: msglen == 127, needs qword to store msglen");
                // i don't really know the byte order, please edit this
                // msglen = BitConverter.ToUInt64(new byte[] { bytes[5], bytes[4], bytes[3], bytes[2], bytes[9], bytes[8], bytes[7], bytes[6] }, 0);
                // offset = 10;
            }

            byte[] decoded = new byte[msglen];
            byte[] masks = new byte[4] { ReadBuffer[offset], ReadBuffer[offset + 1], ReadBuffer[offset + 2], ReadBuffer[offset + 3] };
            offset += 4;

            for (int i = 0; i < msglen; ++i)
                decoded[i] = (byte)(ReadBuffer[offset + i] ^ masks[i % 4]);

            string text = Encoding.UTF8.GetString(decoded, 0, msglen);
            byte[] sendMessage = Encoding.UTF8.GetBytes(text);

            List<byte> sendByteList = new List<byte>();

            BitArray firstInfoArr = new BitArray(
                new bool[]
                {
                    true
                    , false
                    , false
                    , false

                    , false
                    , false
                    , false
                    , true
                }
            );

            byte[] infoArrByte = new byte[1];
            firstInfoArr.CopyTo(infoArrByte, 0);

            sendByteList.Add(infoArrByte[0]);
            sendByteList.Add((byte)text.Length);

            sendByteList.AddRange(sendMessage);

            Client.GetStream().Write(sendByteList.ToArray(), 0, sendByteList.Count);

            Client.GetStream().BeginRead(ReadBuffer, 0, ReadBuffer.Length, OnRecv, null);
        }
    }
}
