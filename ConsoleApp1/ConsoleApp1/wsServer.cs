﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using ConsoleApp1;

namespace ConsoleApp1
{
	class WsServer
	{
		private string Addr { set; get; }
		private int Port { set; get; }
		private TcpListener Listener { set; get; }
		private Dictionary<int, WsClient> ClientList { set; get; }
		private byte[] ReadBuffer { set; get; }
        private int ClientId = 0;

		public WsServer(string ip = "127.0.0.1", int port = 9998)
		{
			Addr = ip;
			Port = port;
            ClientList = new Dictionary<int, WsClient>();

            Listener = new TcpListener(IPAddress.Parse(Addr), Port);
			Listener.Start();

			Listener.BeginAcceptTcpClient(OnConnect, null);
		}

		void OnConnect(IAsyncResult ar)
		{
            int clientId = IssueClientId();
            WsClient client = new WsClient(this, clientId, Listener.EndAcceptTcpClient(ar));

            ClientList.Add(clientId, client);

			Console.WriteLine("new Client Connect");

            client.OnConnect(OnAccept);

			Listener.BeginAcceptTcpClient(OnConnect, null);
		}

		void OnAccept(IAsyncResult ar)
		{
            WsClient client = (WsClient)ar.AsyncState;
            client.OnAccept(ar);
		}

        public void OnDisConnect(int clientId) { ClientList.Remove(clientId); }

        private int IssueClientId() { return ++ClientId; }
	}
}
