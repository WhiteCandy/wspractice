import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class CartService {
    items = [];

    constructor(
        private http: HttpClient
    ) {}

    addToCart(product) {
        console.log("addToCart", product);
        this.items.push(product);
        console.log("addToCart items", this.items);
    }

    getItems() {
        return this.items;
    }

    clearCart() {
        this.items = [];
        return this.items;
    }

    getShippingPrice() {
        return this.http.get('/assets/shipping.json');
    }
}