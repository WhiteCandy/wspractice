import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
    items;

    constructor(
        private cartService: CartService
    ) { }
    
    ngOnInit() {
        console.log("this.cartService.getItems()", this.cartService.getItems());
        this.items = this.cartService.getItems();
        console.log("items", this.items);
    }

}