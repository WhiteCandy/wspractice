import { Component, OnInit } from '@angular/core';
import { WebSocketService } from '../websocket.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html'
})
export class ChatComponent implements OnInit {
    messages = [];
    checkoutForm;

    constructor(
        private wsService: WebSocketService,
        private formBuilder: FormBuilder
    ) {
        this.checkoutForm = this.formBuilder.group({
            userMessage: ''
        });
    }
    
    ngOnInit() {
        this.wsService.setComponent(this);
        if(this.wsService.isConnected() === false)
            this.wsService.connect();
    }

    onSubmit(userMessage) {
        this.wsService.send(userMessage);
        this.checkoutForm.reset();
    }

    onMessage(recvMessage) {
        var message = JSON.parse(recvMessage);
        this.messages.push(message);
    }
}