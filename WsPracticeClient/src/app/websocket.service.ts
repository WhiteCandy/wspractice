import { Injectable } from '@angular/core'

@Injectable({
    providedIn: 'root'
})
export class WebSocketService {
    private socket: WebSocket
    private uri: string
    private component: any

    constructor() {
        this.uri = "ws:\\localhost:9998";
    }

    setComponent(component) {
        this.component = component;
    }

    isConnected(): boolean {
        if(this.socket != null)
            return this.socket.readyState === WebSocket.OPEN;
        else
            return false;
    }

    connect(): void {
        console.log('%cWebSocket: connecting ' + this.uri, 'color: blue;');

        this.socket = new WebSocket(this.uri);

        this.socket.onmessage = (ev: MessageEvent) => {
            this.component.onMessage(ev.data);
        }

        this.socket.onopen = (ev: Event) => {
            console.log('%cWebSocket: opened', 'color:green;');
        }

        this.socket.onclose = (ev: CloseEvent) => {
            console.log('%cWebSocket: closed', 'color: orange;', ev);
            this.disconnect();
        }

        this.socket.onerror = (ev: ErrorEvent) => {
            console.error('WebSocket: error', ev);
        }
    }

    disconnect(): void {
        this.socket.close();
    }

    send(message) {
        var pkt = JSON.stringify(message);
        this.socket.send(pkt);
    }
}